const fs = require("fs");
const path = require("path");

let nbHouses = 0;
let nbFarms = 0;

fs.readdirSync(path.resolve(__dirname, '../basicBuildings'))
  .forEach((buildingFile) => {
    const {orientation, x, y, type, erase} =  require(path.resolve(__dirname, '../basicBuildings', buildingFile));
    if (type === 'HOUSE') nbHouses++;
    if (type === 'FARM') nbFarms++;
  });

console.log(nbFarms/nbHouses);

if (nbHouses > 0 && nbFarms/nbHouses < 0.75) {
    throw new Error('Nombre de fermes insuffisant par rapport au nombre d\'habitations');
}