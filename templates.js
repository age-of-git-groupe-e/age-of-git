module.exports = {
  building: {
    TOWER: {
      motif: ' T ',
      width: 3,
      height: 3
    },
    HOUSE: {
      motif: ' H ',
      width: 2,
      height: 2
    },
    FORGE: {
      motif: ' F ',
      width: 4,
      height: 3
    },
    BARRACK: {
      motif: ' O ',
      width: 5,
      height: 5
    },
    FORUM: {
      motif: ' @ ',
      width: 4,
      height: 4
    },
    BAR: {
      motif: ' $ ',
      width: 4,
      height: 3
    },
    FARM: {
      motif: '###',
      width: 4,
      height: 4
    },
    GATE_H: {
      model: ["[  ", "   ", "  ]"]
    },
    GATE_V: {
      model: ["MMM", "   ", "   ", "WWW"]
    }
  },
  wall: {
    HORIZONTAL: {
      SMALL: {
        motif: '---'
      },
      LARGE: {
        motif: '==='
      }
    },
    VERTICAL: {
      SMALL: {
        motif: ' | '
      },
      LARGE: {
        motif: '|||'
      }
    }
  }
};
