const fs = require("fs");
const path = require("path");

let nbHouses = 0;
let nbBars = 0;

fs.readdirSync(path.resolve(__dirname, '../basicBuildings'))
  .forEach((buildingFile) => {
    const {orientation, x, y, type, erase} =  require(path.resolve(__dirname, '../basicBuildings', buildingFile));
    if (type === 'HOUSE') nbHouses++;
    if (type === 'BAR') nbBars++;
  });

console.log(nbBars/nbHouses);

if (nbHouses > 0 && nbBars/nbHouses < 0.1) {
    throw new Error('Nombre de bars insuffisant par rapport au nombre d\'habitations');
}